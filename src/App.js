import ChatClient from './components/ChatClient';

function App() {
  return (
    <div className="App">
    <ChatClient />
    </div>
  );
}

export default App;
